hyph_fr_FR.dic

Basé sur les motifs de division des mots pour le français diffusé sous licence
LGPL.

<http://www.dicollecte.org/download/fr/hyph-fr-v3.0.zip>

### Pour installer ce dictionnaire sur votre système:

`sudo apt update && sudo apt install hyphen-fr`

`cd /usr/share/hyphen`

déplacer le dict système qu'on vient d'installer:

`sudo mv hyph_fr.dic hyph_fr.dic.orig`

remplacer le fichier par celui-ci https://gitlab.com/medor/hyph_fr/blob/master/hyph_fr_FR.dic

`sudo wget -O hyph_fr.dic https://gitlab.com/medor/hyph_fr/raw/master/hyph_fr_FR.dic`

redémarrer votre browser, OSPkit ou logiciel qui se sert du dict de césures


