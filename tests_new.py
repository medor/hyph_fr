#!/usr/bin/python3
# -*- coding: utf-8 -*-
import pyphen
import unittest


words_1 = {
    'politique'        : 'po-li-ti-que',
    'Politique'        : 'Po-li-ti-que',
    'plastique'        : 'plas-ti-que',
    'cherchent'        : 'cher-chent',
    'caractéristiques' : 'ca-rac-té-ris-ti-ques',
    'paraissent'       : 'pa-rais-sent',
    'femmes'           : 'fem-mes',
    'recherche'        : 're-cher-che',
    'logique'          : 'lo-gi-que',
    'prévoyant'        : 'pré-voyant',
    'tribunal'         : 'tri-bu-nal',
    'vaquent'          : 'va-quent',
    'oncques'          : 'onc-ques',
    "aujourd'hui"      : "au-jour-d'hui",
}

words_2 = {
    # d suivi d'une apostrophe typographique
    "aujourd’hui" : "au-jour-d’hui",
    "prud’homme"  : "pru-d’homme",
    "prud’homal"  : "pru-d’ho-mal",
}

words_3 = {
    # x prononcé s ou z
    "deuxième"              : "deu-xième",
    "deuxièmes"             : "deu-xiè-mes",
    "dixième"               : "di-xième",
    "dixièmes"              : "di-xiè-mes",
    "deuxièmement"          : "deu-xiè-me-ment",
    "dixièmement"           : "di-xiè-me-ment",
    "Bruxelles"             : "Bru-xel-les",
    "bruxellisation"        : "bru-xel-li-sa-tion",
    "bruxellois"            : "bru-xel-lois",
    "bruxelloise"           : "bru-xel-loise",
    "bruxelloises"          : "bru-xel-loi-ses",
    "bruxello-bruxelloises" : "bru-xel-lo-bru-xel-loi-ses",
    "Auxey"                 : "Au-xey",
    "Auxois"                : "Au-xois",
    "auxoise"               : "au-xoise",
    "auxoises"              : "au-xoi-ses",
    "sixième"               : "si-xième",
    "sixièmes"              : "si-xiè-mes",
    "sixièmement"           : "si-xiè-me-ment",
    "soixantaine"           : "soi-xan-taine",
    "soixantaines"          : "soi-xan-tai-nes",
    "soixante"              : "soi-xante",
    "soixantième"           : "soi-xan-tième",
    "soixantièmes"          : "soi-xan-tiè-mes",
    "auxonnais"             : "au-xon-nais",
    "Auxonne"               : "Au-xonne",
    "auxonnaise"            : "au-xon-naise",
    "auxonnaises"           : "au-xon-nai-ses",
    "Auxerre"               : "Au-xerre",
    "auxerroise"            : "au-xer-roise",
    "auxerroises"           : "au-xer-roi-ses",
    "Fixin"                 : "Fi-xin",
    "Aloxe"                 : "Aloxe",
    "Aloxois"               : "Alo-xois",
    "Aloxoise"              : "Alo-xoise",
    "Aloxoises"             : "Alo-xoi-ses",
}

class MyTest(unittest.TestCase):

    def setUp(self):
       #self.dic = pyphen.Pyphen(filename='hyph_fr_FR.dic', left=2, right=2)
        self.dic = pyphen.Pyphen(filename='hyph_fr_MEDOR.dic', left=2, right=2)

    def test_words_1(self):
        #for k, v in words_1.items():
        #    with self.subTest(k=k, v=v):
        #        self.assertEqual(self.dic.inserted(k), v)
        for k, v in words_1.items():
            self.assertEqual(self.dic.inserted(k), v)

    def test_words_2(self):
        for k, v in words_2.items():
            self.assertEqual(self.dic.inserted(k), v)

    def test_words_3(self):
        for k, v in words_3.items():
            self.assertEqual(self.dic.inserted(k), v)


if __name__ == '__main__':
    unittest.main()
